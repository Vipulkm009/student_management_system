package models;

public class Student {
	private String name;
	private String branch;
	private String mobileNumber;
	private String gender;
	
	public Student(String name,String branch, String mobileNumber, String gender) {
		this.name = name;
		this.branch = branch;
		this.mobileNumber = mobileNumber;
		this.gender = gender;
	}
	
	public String getName() {
		return this.name;
	}
	public String getBranch() {
		return this.branch;
	}
	public String getMobileNumber() {
		return this.mobileNumber;
	}
	public String getGender() {
		return this.gender;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String toString() {
		return "'" + this.name + "', '" + this.branch + "', '" + this.mobileNumber + "', '" + this.gender + "'"; 
	}
}
