package views;
import javax.swing.*;
import models.Student;
import services.DatabaseConnection;
import java.awt.*;
import java.awt.event.*;
import java.sql.*;

@SuppressWarnings("serial")
class EnterDataFrame extends JFrame implements ActionListener {
	
	Connection connection;
	Statement statement;
	
	String query;
	
	Student student;
	
	Container container;
	
	JLabel nameLabel;
	JLabel branchLabel;
	JLabel mobileNumberLabel;
	JLabel genderLabel;
	
	JTextField nameTextField;
	JTextField mobileNumberTextField;
	
	JComboBox<String> branchComboBox;
	
	JRadioButton maleButton;
	JRadioButton femaleButton;
	
	ButtonGroup gender;
	
	String[] branchValues = {
			"Computer Science",
			"Mechanical",
			"Electrical",
			"Electronics"
	};
	
	JButton addButton;
	JButton cancelButton;
	
	public EnterDataFrame() {
		try {
			connection = DatabaseConnection.getConnection();
			statement = connection.createStatement();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setTitle("Enter Data");
		setBounds(100, 100, 900, 500);
		
		container = getContentPane();
		container.setLayout(null);
		
		gender = new ButtonGroup();
		
		nameLabel = new JLabel("Name:");
		nameLabel.setBounds(50, 100, 100, 20);
		branchLabel = new JLabel("Branch:");
		branchLabel.setBounds(450, 100, 100, 20);
		mobileNumberLabel = new JLabel("Mobile No.:");
		mobileNumberLabel.setBounds(50, 200, 100, 20);
		genderLabel = new JLabel("Gender:");
		genderLabel.setBounds(450, 200, 100, 20);
		
		nameTextField = new JTextField();
		nameTextField.setBounds(200, 100, 200, 20);
		branchComboBox = new JComboBox<String>(branchValues);
		branchComboBox.setBounds(600, 100, 200, 20);
		mobileNumberTextField = new JTextField();
		mobileNumberTextField.setBounds(200, 200, 200, 20);
		maleButton = new JRadioButton("Male");
		maleButton.setSelected(true);
		maleButton.setBounds(600, 200, 100, 20);
		femaleButton = new JRadioButton("Female");
		femaleButton.setBounds(700, 200, 100, 20);
		
		addButton = new JButton("Add");
		addButton.setBounds(100, 300, 100, 30);
		cancelButton = new JButton("Cancel");
		cancelButton.setBounds(500, 300, 100, 30);
		
		addButton.addActionListener(this);
		cancelButton.addActionListener(this);
		
		container.add(nameLabel);
		container.add(branchLabel);
		container.add(mobileNumberLabel);
		container.add(genderLabel);
		container.add(nameTextField);
		container.add(branchComboBox);
		container.add(mobileNumberTextField);
		container.add(addButton);
		container.add(cancelButton);
		container.add(maleButton);
		container.add(femaleButton);
		
		gender.add(maleButton);
		gender.add(femaleButton);
		
		setVisible(true);
	}
	
	@Override
	public void actionPerformed(ActionEvent event) {
		if(event.getSource().equals(addButton))
		{
			Student student = new Student(
				this.nameTextField.getText(), 
				this.branchComboBox.getItemAt(this.branchComboBox.getSelectedIndex()), 
				this.mobileNumberTextField.getText(), 
				maleButton.isSelected() ? maleButton.getText() : femaleButton.getText()
			);
			nameTextField.setText("");
			branchComboBox.setSelectedIndex(0);
			mobileNumberTextField.setText("");
			maleButton.setSelected(true);
			query = "INSERT INTO student VALUES (" + student.toString() + ")";
			try {
				statement.executeUpdate(query);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else {
//			this.setVisible(false);
			this.dispose();
		}
	}
	
}

public class EnterData {

	public static void main(String[] args) {
		@SuppressWarnings("unused")
		EnterDataFrame frame = new EnterDataFrame();

	}

}
