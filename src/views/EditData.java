package views;
import javax.swing.*;
import models.Student;
import services.DatabaseConnection;
import java.awt.*;
import java.awt.event.*;
import java.sql.*;

@SuppressWarnings("serial")
class EditDataFrame extends JFrame implements ActionListener {
	
	Connection connection;
	ResultSet resultSet;
	Statement statement;
	
	String query;
	
	Student student;
	
	Container container;
	
	JLabel nameLabel;
	JLabel branchLabel;
	JLabel mobileNumberLabel;
	JLabel genderLabel;
	
	JTextField nameTextField;
	JTextField mobileNumberTextField;
	
	JComboBox<String> branchComboBox;
	
	JRadioButton maleButton;
	JRadioButton femaleButton;
	
	ButtonGroup gender;
	
	String[] branchValues = {
			"Computer Science",
			"Mechanical",
			"Electrical",
			"Electronics"
	};
	
	JButton previousButton;
	JButton nextButton;
	JButton updateButton;
	
	public EditDataFrame() {
		try {
			connection = DatabaseConnection.getConnection();
			query = "SELECT * FROM student";
			statement = connection.createStatement();
			resultSet = statement.executeQuery(query);
			if(resultSet.next()) {
				student = new Student(
						resultSet.getString("name"), 
						resultSet.getString("branch"), 
						resultSet.getString("mobileNumber"), 
						resultSet.getString("gender")
				);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setTitle("Enter Data");
		setBounds(100, 100, 900, 500);
		
		container = getContentPane();
		container.setLayout(null);
		
		gender = new ButtonGroup();
		
		nameLabel = new JLabel("Name:");
		nameLabel.setBounds(50, 100, 100, 20);
		branchLabel = new JLabel("Branch:");
		branchLabel.setBounds(450, 100, 100, 20);
		mobileNumberLabel = new JLabel("Mobile No.:");
		mobileNumberLabel.setBounds(50, 200, 100, 20);
		genderLabel = new JLabel("Gender:");
		genderLabel.setBounds(450, 200, 100, 20);
		
		nameTextField = new JTextField(student.getName());
		nameTextField.setBounds(200, 100, 200, 20);
		branchComboBox = new JComboBox<String>(branchValues);
		branchComboBox.setSelectedItem(student.getBranch());
		branchComboBox.setBounds(600, 100, 200, 20);
		mobileNumberTextField = new JTextField(student.getMobileNumber());
		mobileNumberTextField.setBounds(200, 200, 200, 20);
		maleButton = new JRadioButton("Male");
		maleButton.setBounds(600, 200, 100, 20);
		femaleButton = new JRadioButton("Female");
		femaleButton.setBounds(700, 200, 100, 20);
		if(!student.getBranch().equals(maleButton.getText())) {
			maleButton.setSelected(true);
		}
		else {
			femaleButton.setSelected(true);
		}
		
		previousButton = new JButton("Previous");
		previousButton.setBounds(100, 300, 100, 30);
		nextButton = new JButton("Next");
		nextButton.setBounds(500, 300, 100, 30);
		updateButton = new JButton("Update");
		updateButton.setBounds(300, 300, 100, 30);
		
		
		previousButton.addActionListener(this);
		nextButton.addActionListener(this);
		updateButton.addActionListener(this);
		
		container.add(nameLabel);
		container.add(branchLabel);
		container.add(mobileNumberLabel);
		container.add(genderLabel);
		container.add(nameTextField);
		container.add(branchComboBox);
		container.add(mobileNumberTextField);
		container.add(previousButton);
		container.add(nextButton);
		container.add(maleButton);
		container.add(femaleButton);
		container.add(updateButton);
		
		gender.add(maleButton);
		gender.add(femaleButton);
		
		setVisible(true);
	}
	
	public void actionPerformed(ActionEvent event) {
		if(event.getSource().equals(nextButton))
		{
			try {
				if(resultSet.next()) {
					if(!previousButton.isEnabled()) {
						previousButton.setEnabled(true);
					}
					if(resultSet.isLast()) {
						nextButton.setEnabled(false);
					}
					else {
						nextButton.setEnabled(true);
					}
					student = new Student(
							resultSet.getString("name"), 
							resultSet.getString("branch"), 
							resultSet.getString("mobileNumber"), 
							resultSet.getString("gender")
					);
					nameTextField.setText(student.getName());
					branchComboBox.setSelectedItem(student.getBranch());
					mobileNumberTextField.setText(student.getMobileNumber());
					if(student.getGender().equals("Male")) {
						maleButton.setSelected(true);
					}
					else {
						femaleButton.setSelected(true);
					}
				}
				else {
					nextButton.setEnabled(false);
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else if(event.getSource().equals(updateButton)) {
			Student stu = new Student(
				this.nameTextField.getText(), 
				this.branchComboBox.getItemAt(this.branchComboBox.getSelectedIndex()), 
				this.mobileNumberTextField.getText(), 
				maleButton.isSelected() ? maleButton.getText() : femaleButton.getText()
			);
			query = "DELETE FROM student WHERE name = '" + student.getName() + "' and mobileNumber = '" + student.getMobileNumber() + "'";
			try {
				statement.executeUpdate(query);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			query = "INSERT INTO student VALUES (" + stu.toString() + ")";
			try {
				statement.executeUpdate(query);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				query = "SELECT * FROM student";
				statement = connection.createStatement();
				resultSet = statement.executeQuery(query);
			} catch (SQLException e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
		else {
				try {
					if(resultSet.previous()) {
						if(!nextButton.isEnabled()) {
							nextButton.setEnabled(true);
						}
						if(resultSet.isFirst()) {
							previousButton.setEnabled(false);
						}
						else {
							previousButton.setEnabled(true);
						}
						student = new Student(
								resultSet.getString("name"), 
								resultSet.getString("branch"), 
								resultSet.getString("mobileNumber"), 
								resultSet.getString("gender")
						);
						nameTextField.setText(student.getName());
						branchComboBox.setSelectedItem(student.getBranch());
						mobileNumberTextField.setText(student.getMobileNumber());
						if(student.getGender().equals("Male")) {
							maleButton.setSelected(true);
						}
						else {
							femaleButton.setSelected(true);
						}
					}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}

public class EditData {

	public static void main(String[] args) {
		@SuppressWarnings("unused")
		EditDataFrame frame = new EditDataFrame();

	}

}
