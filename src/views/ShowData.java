package views;
import javax.swing.*;

import models.Student;
import services.DatabaseConnection;
import java.awt.*;
import java.awt.event.*;
import java.sql.*;


@SuppressWarnings("serial")
class ShowDataFrame extends JFrame implements ActionListener {
	
	Connection connection;
	ResultSet resultSet;
	Statement statement;
	
	String query;
	
	Student student;
	
	Container container;
	
	JLabel nameLabel;
	JLabel branchLabel;
	JLabel mobileNumberLabel;
	JLabel genderLabel;
	
	JLabel nameValueLabel;
	JLabel mobileNumberValueLabel;
	JLabel branchValueLabel;
	JLabel genderValueLabel;
	
	JButton previousButton;
	JButton nextButton;
	
	public ShowDataFrame() {
		try {
			connection = DatabaseConnection.getConnection();
			query = "SELECT * FROM student";
			statement = connection.createStatement();
			resultSet = statement.executeQuery(query);
			if(resultSet.next()) {
				student = new Student(
						resultSet.getString("name"), 
						resultSet.getString("branch"), 
						resultSet.getString("mobileNumber"), 
						resultSet.getString("gender")
				);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setTitle("Enter Data");
		setBounds(100, 100, 900, 500);
		
		container = getContentPane();
		container.setLayout(null);
		
		nameLabel = new JLabel("Name:");
		nameLabel.setBounds(50, 100, 100, 20);
		branchLabel = new JLabel("Branch:");
		branchLabel.setBounds(450, 100, 100, 20);
		mobileNumberLabel = new JLabel("Mobile No.:");
		mobileNumberLabel.setBounds(50, 200, 100, 20);
		genderLabel = new JLabel("Gender:");
		genderLabel.setBounds(450, 200, 100, 20);
		
		Font font = new Font("Arial", Font.ITALIC, 20);
		nameValueLabel = new JLabel(student.getName());
		nameLabel.setForeground(Color.BLACK);
		nameValueLabel.setFont(font);
		nameValueLabel.setBounds(200, 100, 200, 20);
		branchValueLabel = new JLabel(student.getBranch());
		branchValueLabel.setForeground(Color.BLACK);
		branchValueLabel.setFont(font);
		branchValueLabel.setBounds(600, 100, 200, 20);
		mobileNumberValueLabel = new JLabel(student.getMobileNumber());
		mobileNumberValueLabel.setForeground(Color.BLACK);
		mobileNumberValueLabel.setFont(font);
		mobileNumberValueLabel.setBounds(200, 200, 200, 20);
		genderValueLabel = new JLabel(student.getGender());
		genderValueLabel.setForeground(Color.BLACK);
		genderValueLabel.setFont(font);
		genderValueLabel.setBounds(600, 200, 200, 20);
		
		previousButton = new JButton("Previous");
		previousButton.setEnabled(false);
		previousButton.setBounds(100, 300, 100, 30);
		nextButton = new JButton("Next");
		try {
			if(resultSet.isLast())
				nextButton.setEnabled(false);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		nextButton.setBounds(500, 300, 100, 30);
		
		previousButton.addActionListener(this);
		nextButton.addActionListener(this);
		
		container.add(nameLabel);
		container.add(branchLabel);
		container.add(mobileNumberLabel);
		container.add(genderLabel);
		container.add(nameValueLabel);
		container.add(branchValueLabel);
		container.add(mobileNumberValueLabel);
		container.add(genderValueLabel);
		container.add(previousButton);
		container.add(nextButton);
		
		setVisible(true);
	}
	
	@Override
	public void actionPerformed(ActionEvent event) {
		if(event.getSource().equals(nextButton))
		{
			try {
				if(resultSet.next()) {
					if(!previousButton.isEnabled()) {
						previousButton.setEnabled(true);
					}
					if(resultSet.isLast()) {
						nextButton.setEnabled(false);
					}
					else {
						nextButton.setEnabled(true);
					}
					student = new Student(
							resultSet.getString("name"), 
							resultSet.getString("branch"), 
							resultSet.getString("mobileNumber"), 
							resultSet.getString("gender")
					);
					nameValueLabel.setText(student.getName());
					branchValueLabel.setText(student.getBranch());
					mobileNumberValueLabel.setText(student.getMobileNumber());
					genderValueLabel.setText(student.getGender());
				}
				else {
					nextButton.setEnabled(false);
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else {
				try {
					if(resultSet.previous()) {
						if(!nextButton.isEnabled()) {
							nextButton.setEnabled(true);
						}
						if(resultSet.isFirst()) {
							previousButton.setEnabled(false);
						}
						else {
							previousButton.setEnabled(true);
						}
						student = new Student(
								resultSet.getString("name"), 
								resultSet.getString("branch"), 
								resultSet.getString("mobileNumber"), 
								resultSet.getString("gender")
						);
						nameValueLabel.setText(student.getName());
						branchValueLabel.setText(student.getBranch());
						mobileNumberValueLabel.setText(student.getMobileNumber());
						genderValueLabel.setText(student.getGender());
					}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
}

public class ShowData {

	public static void main(String []args) {
		ShowDataFrame frame = new ShowDataFrame();
	}
}
