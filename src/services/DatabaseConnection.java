package services;
import java.sql.*;
//import java.sql.Connection;
//import java.sql.DriverManager;
//import java.sql.SQLException;

public class DatabaseConnection {
  //static reference to itself
  private static DatabaseConnection instance = new DatabaseConnection();
  public static final String URL = "jdbc:mysql://localhost:3306/student_management_system";
  public static final String USER = "root";
  public static final String PASSWORD = "root"; 
   
  private DatabaseConnection() {
      try {
          Class.forName("com.mysql.jdbc.Driver");
      } catch (ClassNotFoundException e) {
          e.printStackTrace();
      }
  }
   
  private Connection createConnection() {

      Connection connection = null;
      try {
          connection = DriverManager.getConnection(URL, USER, PASSWORD);
      } catch (SQLException e) {
          System.out.println("ERROR: Unable to Connect to Database.");
          System.out.println("Reason: " + e.toString());
      }
      return connection;
  }   
   
  public static Connection getConnection() {
      return instance.createConnection();
  }
}